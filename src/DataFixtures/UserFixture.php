<?php

namespace App\DataFixtures;

use App\Entity\Image;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserFixture extends Fixture implements FixtureGroupInterface
{
    private UserPasswordEncoderInterface $passwordEncoder;
    private string $upload_target_dir;
    private string $img_path;

    public function __construct(UserPasswordEncoderInterface $passwordEncoder, string $upload_target_dir, string $img_path)
    {
        $this->passwordEncoder = $passwordEncoder;
        $this->upload_target_dir = $upload_target_dir;
        $this->img_path = $img_path;
    }

    public function load(ObjectManager $manager)
    {
        $faker = Factory::create('en_EN');
        for ($i = 0; $i < 5; ++$i) {
            $user = new User();
            $user->setFirstName($faker->firstName);
            $user->setLastName($faker->lastName);
            $user->setUserName($faker->userName);
            $user->setEmail($faker->email);
            $user->setIsVerified(true);
            $fakerPic = $faker->image($this->upload_target_dir, 250, 250, 'people', false, true);
            $profilePic = (new Image())->setName($fakerPic)
                ->setThumbPath($this->img_path.'original/'.$fakerPic)
                ->setMediumPath($this->img_path.'original/'.$fakerPic)
                ->setOriginalPath($this->img_path.'original/'.$fakerPic)
            ;
            $manager->persist($profilePic);
            $user->setProfilePic($profilePic);
            $manager->persist($user);
            $user->setPassword($this->passwordEncoder->encodePassword(
                $user,
                $user->getFirstName()
            ));
        }
        $manager->flush();
    }

    public static function getGroups(): array
    {
        return ['UserFixture'];
    }
}
