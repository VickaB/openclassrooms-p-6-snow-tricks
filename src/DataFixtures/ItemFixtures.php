<?php

namespace App\DataFixtures;

use App\Entity\Image;
use App\Entity\Trick;
use App\Repository\GroupeRepository;
use App\Repository\UserRepository;
use App\Repository\VideoRepository;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;
use Symfony\Component\String\Slugger\SluggerInterface;

class ItemFixtures extends Fixture implements DependentFixtureInterface
{
    private UserRepository $userRepository;
    private GroupeRepository $groupRepository;
    private VideoRepository $videoRepository;
    private SluggerInterface $slugger;

    public function __construct(
        UserRepository $userRepository,
        GroupeRepository $groupRepository,
        VideoRepository $videoRepository,
        SluggerInterface $slugger
    ) {
        $this->userRepository = $userRepository;
        $this->groupRepository = $groupRepository;
        $this->videoRepository = $videoRepository;
        $this->slugger = $slugger;
    }

    public function load(ObjectManager $manager)
    {
        $faker = Factory::create('en_EN');
        $users = $this->userRepository->findAll();
        $groups = $this->groupRepository->findAll();
        $videos = [
            ['name' => 'my youtube video', 'url' => 'https://youtu.be/b4sakyx-VoY'],
            ['name' => 'my vimeo video', 'url' => 'https://vimeo.com/6097400'],
            ['name' => 'my daily video', 'url' => 'https://dai.ly/x7sgfx5'],
        ];
        $tricks = [];

        for ($i = 0; $i < 20; ++$i) {
            shuffle($users);
            shuffle($groups);
            $item = new Trick(uniqid('Trick_'.$faker->word.'_'), $users[0]);
            $manager->persist($item);
            $manager->flush();
            $highlight = (new Image())
                ->setName('trick_'.$item->getSlug().'_highlight')
                ->setOriginalPath('https://picsum.photos/id/256/2000/697')
                ->setMediumPath('https://picsum.photos/id/256/1000/397')
                ->setThumbPath('https://picsum.photos/id/256/300/300')
            ;

            $manager->persist($highlight);
            $item->setGroupe($groups[0])->setHighlight($highlight)->setDescription($faker->text(1200));
            $manager->persist($item);
            $tricks[] = $item;
        }
        foreach ($tricks as $trick) {
            for ($i = 0; $i < 10; ++$i) {
                $image = (new Image())->setName('Image'.$i.'_image_box'.$trick->getImageBox()->getId())
                        ->setOriginalPath('https://picsum.photos/id/256/2000/697')
                        ->setMediumPath('https://picsum.photos/id/256/1000/397')
                        ->setThumbPath('https://picsum.photos/id/256/300/300');
                $manager->persist($image);
                $trick->getImageBox()->addMedium($image);
                shuffle($videos);
                $video = $this->videoRepository->create($videos[0]);
                $trick->getVideoBox()->addMedium($video);
                $manager->persist($trick);
            }
        }

        $manager->flush();
    }

    public function getDependencies()
    {
        return [
            GroupeFixtures::class,
            UserFixture::class,
        ];
    }

    public static function getGroups(): array
    {
        return ['ItemFixture'];
    }
}
