<?php

namespace App\Interfaces;

interface MediaTypeInterface
{
    public function getType(): string;
    public function getId(): int;
}
