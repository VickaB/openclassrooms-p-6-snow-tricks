<?php

namespace App\Interfaces;

use DateTimeInterface;
use Doctrine\Common\Collections\Collection;

interface GalleryInterface extends MediaTypeInterface
{
    public function getId(): int;

    public function getType(): string;

    public function getCreatedAt(): ?DateTimeInterface;

    public function getMedia(): Collection;

    public function addMedium(MediaTypeInterface $mediaType): ?MediaTypeInterface;

    public function removeMedium(MediaTypeInterface $mediaType): self;
}
