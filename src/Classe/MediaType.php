<?php

namespace App\Classe;

abstract class MediaType
{
    public const MEDIA_IMAGE = 'Image';
    public const MEDIA_VIDEO = 'Video';
}
