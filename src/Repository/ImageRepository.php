<?php

namespace App\Repository;

use App\Entity\Image;
use App\Entity\ImageBox;
use App\Service\Events\Dispatcher\ImageBoxMediaEvent;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\ORMException;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Contracts\EventDispatcher\EventDispatcherInterface;

/**
 * @method Image|null find($id, $lockMode = null, $lockVersion = null)
 * @method Image|null findOneBy(array $criteria, array $orderBy = null)
 * @method Image[]    findAll()
 * @method Image[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ImageRepository extends ServiceEntityRepository
{
    private EventDispatcherInterface $dispatcher;
    public const PAGINATOR_PER_PAGE = 6;

    public function __construct(ManagerRegistry $registry, EventDispatcherInterface $dispatcher)
    {
        parent::__construct($registry, Image::class);
        $this->dispatcher = $dispatcher;
    }

    public function save(Image $element): bool
    {
        try {
            $this->_em->persist($element);
            $this->_em->flush();
        } catch (ORMException $e) {
            return false;
        }

        return true;
    }

//    public function getPaginator(int $offset)
//    {
//        $builder = $this->createQueryBuilder('i')
//            //->innerJoin('a.user', 'u', 'WITH', 'u = :user')
//            ->innerJoin('ib.media', 'ib', 'WITH', 'cu = :user')
//            ->where('a.active = :active')
//            ->setParameters([
//                'user' => $this->getUser(),
//                'active' => 1,
//            ]);
//        $builder->setFirstResult($offset)->setMaxResults(self::PAGINATOR_PER_PAGE);
//        $query = $builder->getQuery();
//        $paginator = new Paginator($query);
//	    $totalResult = $paginator->count();
//	    $tricks = $paginator->getIterator()->getArrayCopy();
//    }

    public function throwImageBoxMediaEvent(ImageBox $imageBox, Image $image)
    {
        $this->dispatcher
            ->dispatch(new ImageBoxMediaEvent($imageBox, $image), ImageBoxMediaEvent::MEDIA_IMAGE_ADDED);
    }

    // /**
    //  * @return Image[] Returns an array of Image objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('i')
            ->andWhere('i.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('i.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Image
    {
        return $this->createQueryBuilder('i')
            ->andWhere('i.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
