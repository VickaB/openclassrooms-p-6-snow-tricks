<?php

namespace App\Repository;

use App\Entity\ImageBox;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\ORMException;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ImageBox|null find($id, $lockMode = null, $lockVersion = null)
 * @method ImageBox|null findOneBy(array $criteria, array $orderBy = null)
 * @method ImageBox[]    findAll()
 * @method ImageBox[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ImageBoxRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ImageBox::class);
    }

    public function save(ImageBox $element): bool
    {
        try {
            $this->_em->persist($element);
            $this->_em->flush();
        } catch (ORMException $e) {
            return false;
        }

        return true;
    }

    // /**
    //  * @return ImageBox[] Returns an array of ImageBox objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('g')
            ->andWhere('g.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('g.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ImageBox
    {
        return $this->createQueryBuilder('g')
            ->andWhere('g.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
