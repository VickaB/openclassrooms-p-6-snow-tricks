<?php

namespace App\Repository;

use App\Entity\VideoBox;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\ORMException;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method VideoBox|null find($id, $lockMode = null, $lockVersion = null)
 * @method VideoBox|null findOneBy(array $criteria, array $orderBy = null)
 * @method VideoBox[]    findAll()
 * @method VideoBox[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class VideoBoxRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, VideoBox::class);
    }

    public function save(VideoBox $element)
    {
        try {
            $this->_em->persist($element);
            $this->_em->flush();
        } catch (ORMException $e) {
            return false;
        }

        return true;
    }

    // /**
    //  * @return VideoBox[] Returns an array of VideoBox objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('v')
            ->andWhere('v.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('v.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?VideoBox
    {
        return $this->createQueryBuilder('v')
            ->andWhere('v.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
