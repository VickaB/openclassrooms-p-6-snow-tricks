<?php

namespace App\Controller;

use App\Entity\Image;
use App\Entity\Trick;
use App\Entity\User;
use App\Form\TrickType;
use App\Repository\TrickRepository;
use App\Service\SimpleImage;
use DateTime;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route({
 *     "en":"/trick",
 *     "fr":"/fr/trick",
 *     "de":"/de/trick"
 * })
 */
class TrickController extends AbstractController
{
    private ?Request $request;
    private TrickRepository $repository;
    private SimpleImage $simpleImage;

    public function __construct(
        TrickRepository $repository,
        SimpleImage $simpleImage,
        RequestStack $requestStack
    ) {
        $this->request = $requestStack->getCurrentRequest();
        $this->repository = $repository;
        $this->simpleImage = $simpleImage;
    }

    /**
     * @Route("/", name="trick_index", methods={"GET", "POST"})
     */
    public function index(): Response
    {
        $offset = max(0, $this->request->query->getInt('offset', 0));
        $paginator = $this->repository->getPaginator($offset);
        $tricks = $paginator['tricks'];
        $total = $paginator['total'];

        return $this->render('trick/index.html.twig', [
            'tricks' => $tricks,
            'offset' => $offset,
            'total' => $total,
            'next' => min($total, $offset + TrickRepository::PAGINATOR_PER_PAGE),
        ]);
    }

    /**
     * @Route("/list", name="trick_list", methods={"GET"})
     */
    public function getList(): Response
    {
        $offset = max(0, $this->request->query->getInt('offset', 0));
        $paginator = $this->repository->getPaginator($offset);
        $tricks = $paginator['tricks'];
        $total = $paginator['total'];

        return $this->render('trick/_list.html.twig', [
            'target' => $this->request->query->get('target'),
            'tricks' => $tricks,
            'total' => $total,
            'previous' => $offset - TrickRepository::PAGINATOR_PER_PAGE,
            'next' => min($total, $offset + TrickRepository::PAGINATOR_PER_PAGE),
        ]);
    }

    /**
     * @Route("/new", name="trick_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');
        /**
         * @var $user User
         */
        $user = $this->getUser();
        $trick = new Trick(uniqid('Trick_'), $user);
        $highlight = new Image();
        $form = $this->createForm(TrickType::class, $trick);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $trick->setAuthor($user);
            $trick->setLastUpdateDateTime(new DateTime('now'));
            $entityManager = $this->getDoctrine()->getManager();
            if ($imageFile = $form['highlight']->getData()) {
                $this->simpleImage->treat($imageFile, $highlight);
                $trick->setHighlight($highlight);
                $entityManager->persist($highlight);
            }
            $entityManager->persist($trick);
            $entityManager->flush();
            $this->addFlash('success', $trick->getName().' succesfully added!');

            return $this->redirectToRoute('trick_add_media', ['slug' => $trick->getSlug()]);
        }

        return $this->render('trick/new.html.twig', [
            'trick' => $trick,
            'formTrick' => $form->createView(),
        ]);
    }

    /**
     * @Route("/add-media/{slug}", name="trick_add_media", methods={"GET"})
     */
    public function trickAddMedia(Trick $trick): Response
    {
        return $this->render('trick/add_media.html.twig', [
            'trick' => $trick,
        ]);
    }

    /**
     * @Route("/card/{slug}", name="trick_card_show", methods={"GET"})
     */
    public function showCard(Trick $trick): Response
    {
        return $this->render('trick/show.html.twig', [
            'trick' => $trick,
        ]);
    }

    /**
     * @Route("/{slug}", name="trick", methods={"GET"})
     */
    public function show(Trick $trick): Response
    {
        return $this->render('trick/show.html.twig', [
            'trick' => $trick,
        ]);
    }

    /**
     * @Route("/{slug}/edit", name="trick_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Trick $trick): Response
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');
        $formTrick = $this->createForm(TrickType::class, $trick);
        $formTrick->handleRequest($request);
        $highlight = $trick->getHighlight() ? $trick->getHighlight() : new Image();
        if ($formTrick->isSubmitted()) {
            if ($formTrick->isValid()) {
                if ($imageFile = $formTrick['highlight']->getData()) {
                    $this->simpleImage->treat($imageFile, $highlight);
                    $trick->setHighlight($highlight);
                    $trick->setLastUpdateDateTime(new DateTime('now'));
                }
                $this->getDoctrine()->getManager()->flush();
                $this->addFlash('success', $trick->getName().' successfully edited!');

                return $this->redirectToRoute('trick_add_media', ['slug' => $trick->getSlug()]);
            }
            $this->addFlash('error', 'Sorry, something wrong happened, please, try again.');
        }

        return $this->render('trick/edit.html.twig', [
            'trick' => $trick,
            'formTrick' => $formTrick->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="trick_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Trick $trick): Response
    {
        if ($this->isCsrfTokenValid('delete'.$trick->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($trick);
            $entityManager->flush();
        }

        return $this->redirectToRoute('trick_index');
    }
}
