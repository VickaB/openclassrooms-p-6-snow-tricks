<?php

namespace App\Controller;

use App\Entity\Image;
use App\Entity\ImageBox;
use App\Repository\ImageBoxRepository;
use App\Repository\ImageRepository;
use App\Service\ImageToBox;
use DateTime;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route({
 *     "en":"/image-box",
 *     "fr":"/fr/image-box",
 *     "de":"/de/image-box"
 * })
 */
class ImageBoxController extends AbstractController
{
    private ImageToBox $imageToBox;
    private ImageBoxRepository $repository;
    private ?Request $request;

    public function __construct(ImageToBox $imageToBox, ImageBoxRepository $repository, RequestStack $requestStack)
    {
        $this->imageToBox = $imageToBox;
        $this->repository = $repository;
        $this->request = $requestStack->getCurrentRequest();
    }


    /**
     * @Route("/{id}", name="image_box_show", methods={"GET"})
     */
    public function show(ImageBox $imageBox): Response
    {
        $offset = max(0, $this->request->query->getInt('offset', 0));
        $media = $imageBox->getPaginatedMedia($offset, 4);
        $total = $imageBox->getMedia()->count();

        return $this->render('image_box/show.html.twig', [
            'total' => $total,
            'previous' => $offset - 1,
            'next' => min($total, $offset + 1),
            'media' => $media,
            'image_box' => $imageBox,
            'formMediaImage' => $this->imageToBox->form(null, $imageBox, 'new_media_image'),
        ]);
    }

    /**
     * @Route("/list/{id}", name="image_media_list", methods={"GET"})
     */
    public function getList(ImageBox $imageBox): Response
    {
        $offset = max(0, $this->request->query->getInt('offset', 0));
        $media = $imageBox->getPaginatedMedia($offset, 4);
        $total = $imageBox->getMedia()->count();

        return $this->render('image_box/_media_list.html.twig', [
            'image_box' => $imageBox,
            'media' => $media,
            'total' => $total,
            'previous' => $offset - 1,
            'next' => min($total, $offset + 1),
        ]);
    }

    /**
     * @Route("/{id}/edit", name="image_box_edit", methods={"GET","POST"})
     */
    public function edit(ImageBox $imageBox): Response
    {
        $offset = max(0, $this->request->query->getInt('offset', 0));
        $media = $imageBox->getPaginatedMedia($offset, 4);
        $total = $imageBox->getMedia()->count();

        return $this->render('image_box/edit.html.twig', [
            'total' => $total,
            'previous' => $offset - 1,
            'next' => min($total, $offset + 1),
            'media' => $media,
            'image_box' => $imageBox,
        ]);
    }

    /**
     * @Route("/render-media-image-form/{image_box_id}",
     *     name="render_add_media_image_form", methods={"GET","POST"})
     * @Route("/render-media-image-form/{image}/{image_box_id}",
     *      name="render_edit_media_image_form", methods={"GET","POST"})
     */
    public function renderMediaImageForm(?Image $image, Request $request): Response
    {
        $imageBox = $this->repository->find($request->get('image_box_id'));
        $mediaAction = $request->get('media_action');
        $image = !empty($image) ? $image : null;
        $formMedia = $this->imageToBox->form($image, $imageBox, $mediaAction);

        return  $this->render('image_box/_media_image_form.html.twig', [
            'media_action' => $mediaAction,
            'medium' => $image,
            'formMediaImage' => $formMedia,
        ]);
    }

    /**
     * @Route("/{image_box_id}/new-media-image", name="new_media_image", methods={"GET","POST"})
     * @Route("/{image_box_id}/edit-media-image/{image_id}", name="edit_media_image", methods={"GET","POST"})
     */
    public function mediaImage(Request $request, ImageToBox $imageToBox, ImageRepository $imageRepository): Response
    {
        $key = $request->request->keys()[0];
        $imageBox = $this->repository->find($request->get('image_box_id'));
        if (!empty($request->files->get($key))) {
            $imgFile = $request->files->get($key)['media_file'];
            if ('edit_media_image' == $request->get('_route')) {
                $oldImage = $imageRepository->find($request->get('image_id'));
                $imageToBox->update($oldImage, $imgFile);
            } else {
                $imageToBox->addMedia($imageBox, $imgFile);
            }
        }
        $trick = $imageBox->getTrick();
        $trick->setLastUpdateDateTime(new DateTime('now'));
        $this->getDoctrine()->getManager()->flush();

        return $this->redirectToRoute('trick_add_media', ['slug' => $imageBox->getTrick()->getSlug()]);
    }

    /**
     * @Route("/{imageBox}/{image}", name="image_box_delete_media", methods={"DELETE"})
     */
    public function deleteMedia(Request $request, ImageBox $imageBox, Image $image): Response
    {
        if ($this->isCsrfTokenValid('delete'.$imageBox->getId().$image->getId(), $request->request->get('_token'))) {
            $imageBox->removeMedium($image);
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($image);
            $entityManager->flush();
        }

        return $this->redirectToRoute('trick_edit', ['slug' => $imageBox->getTrick()->getSlug()]);
    }
}
