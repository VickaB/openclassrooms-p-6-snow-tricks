<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\RegistrationFormType;
use App\Repository\UserRepository;
use App\Service\Mailer;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Core\Exception\CustomUserMessageAccountStatusException;

class RegistrationController extends AbstractController
{
    private string $email_admin;

    public function __construct(string $email_admin)
    {
        $this->email_admin = $email_admin;
    }

    /**
     * @Route("/register", name="app_register")
     */
    public function register(Request $request, UserPasswordEncoderInterface $passwordEncoder, Mailer $mailer): Response
    {
        $user = new User();
        $form = $this->createForm(RegistrationFormType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                $user->setPassword(
                    $passwordEncoder->encodePassword(
                        $user,
                        $form->get('plainPassword')->getData()
                    )
                );

                $entityManager = $this->getDoctrine()->getManager();
                $entityManager->persist($user);
                $entityManager->flush();

                $mailer->sendEmail(
                    'Welcome To SnowTricks - Validate You Email',
                    $this->email_admin,
                    $user->getEmail(),
                    'registration/confirmation_email.html.twig',
                    ['user' => $user]
                );
                $this->addFlash('success', 'Your Account is created, check your mailbox for mail validation.');

                return $this->redirectToRoute('index');
            }
            $this->addFlash('error', 'Something went wrong, please try again.');
        }

        return $this->render('registration/register.html.twig', [
            'registrationForm' => $form->createView(),
        ]);
    }

    /**
     * @Route("/email-validation", name="app_email_validation")
     */
    public function validateEmail(Request $request, UserRepository $userRepository): RedirectResponse
    {
        try {
            /**
             * @var User $user
             */
            if (!$user = $userRepository->find($request->get('user'))) {
                throw new CustomUserMessageAccountStatusException("We couldn't find this account");
            }
            if ($user->isVerified()) {
                throw new CustomUserMessageAccountStatusException('This email is already verified');
            }
            if (!$user->setIsVerified(true)) {
                throw new CustomUserMessageAccountStatusException("We couldn't validate your email");
            }
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($user);
            $entityManager->flush();
            $this->addFlash(
                'verify_email_success',
                'Congratulations! Your E-mail adress has been successfully validated!'
            );

            return $this->redirectToRoute('app_login');
        } catch (CustomUserMessageAccountStatusException $e) {
            $this->addFlash('verify_email_error', $e->getMessage());

            return $this->redirectToRoute('index');
        }
    }
}
