<?php

namespace App\Controller;

use App\Entity\Comment;
use App\Entity\Trick;
use App\Form\CommentType;
use App\Repository\CommentRepository;
use App\Repository\TrickRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route({
 *     "en":"/comment",
 *     "fr":"/fr/comment",
 *     "de":"/de/comment"
 * })
 */
class CommentController extends AbstractController
{
    private CommentRepository $commentRepository;
    private TrickRepository $trickRepository;

    public function __construct(CommentRepository $commentRepository, TrickRepository $trickRepository)
    {
        $this->trickRepository = $trickRepository;
        $this->commentRepository = $commentRepository;
    }

    /**
     * @Route("/add/{action}" , name="comment_add", methods={"POST"})
     */
    public function add(Request $request): Response
    {
        //@Todo add markdown
        //@Todo sanitize and add akismet
        $data = $request->get('comment');
        $comment = new Comment();
        $comment->setTrick($this->trickRepository->findOneBy(['id' => $data['trick']]));
        $comment->setAuthor($this->getUser());
        $comment->setMessage($data['message']);
        if (!empty($data['parent']) && '0' !== $data['parent']) {
            $comment->setParent($this->commentRepository->find($data['parent']));
        }
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($comment);
        $entityManager->flush();

        return $this->redirectToRoute('trick', ['slug' => $comment->getTrick()->getSlug()]);
    }

    /**
     * @Route("/{commentId}", name="comment_show", methods={"GET"})
     */
    public function show(Comment $comment): Response
    {
        return $this->render('comment/show.html.twig', [
            'comment' => $comment,
            'comments' => $this->commentRepository->findAll(),
        ]);
    }

    /**
     * @Route("/form", name="comment_form", methods={"POST"})
     */
    public function form(Request $request, ?Comment $parent, Trick $trick): Response
    {
        $action = $request->get('action');
        $form = $this->createForm(
            CommentType::class,
            null,
            ['action' => $this->generateUrl('comment_add', ['action' => $action])]
        );
        $form->handleRequest($request);

        return $this->render('comment/_form.html.twig', [
            'parent' => $parent ? $parent->getId() : null,
            'trick' => $trick,
            'action' => $action,
            'formComment' => $form->createView(),
        ]);
    }

//    /**
//     * @Route("/{commentId}", name="comment_delete", methods={"DELETE"})
//     */
//    public function delete(Request $request, Comment $comment): Response
//    {
//        if ($this->isCsrfTokenValid('delete'.$comment->getId(), $request->request->get('_token'))) {
//            $entityManager = $this->getDoctrine()->getManager();
//            $entityManager->remove($comment);
//            $entityManager->flush();
//        }
//
//        return $this->redirectToRoute('comment_index');
//    }
}
