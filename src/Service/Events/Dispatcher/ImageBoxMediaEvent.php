<?php


namespace App\Service\Events\Dispatcher;

use App\Entity\Image;
use App\Entity\ImageBox;
use Symfony\Contracts\EventDispatcher\Event;

class ImageBoxMediaEvent extends Event
{
    public const MEDIA_IMAGE_ADDED = 'box.addNewMediaImage';

    private ImageBox $imageBox;
    private Image $image;

    /**
     * ImageBoxMediaEvent constructor.
     * @param ImageBox $imageBox
     * @param Image $image
     */
    public function __construct(ImageBox $imageBox, Image $image)
    {
        $this->imageBox = $imageBox;
        $this->image = $image;
    }

    /**
     * @return mixed
     */
    public function getBox()
    {
        return $this->imageBox;
    }

    /**
     * @return mixed
     */
    public function getImage()
    {
        return $this->image;
    }
}
