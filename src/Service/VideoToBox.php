<?php

namespace App\Service;

use App\Entity\Video;
use App\Entity\VideoBox;
use App\Form\MediaVideoBoxType;
use App\Repository\VideoRepository;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class VideoToBox
{
    private VideoRepository $repository;
    private FormFactoryInterface $formFactory;
    private UrlGeneratorInterface $routeur;

    public function __construct(
        VideoRepository $repository,
        FormFactoryInterface $formFactory,
        UrlGeneratorInterface $routeur
    ) {
        $this->repository = $repository;
        $this->formFactory = $formFactory;
        $this->routeur = $routeur;
    }

    public function form(?Video $video, VideoBox $videoBox, string $mediaAction)
    {
        $videoId = !$video ? null : $video->getId();
        $formName = uniqid('video_box_'.$videoBox->getId().'_video'.$videoId.'_');
        $form = $this->formFactory->createNamed(
            $formName,
            MediaVideoBoxType::class,
            $videoBox,
            ['action' => $this->routeur->generate($mediaAction, [
                'video_box_id' => $videoBox->getId(),
                'video_id' => $videoId, ])]
        );

        return $form->createView();
    }

    public function addMedia(VideoBox $videoBox, $newVideo): Video
    {
        $video = $this->repository->create($newVideo);
        $this->repository->throwVideoBoxMediaEvent($videoBox, $video);

        return $video;
    }
}
