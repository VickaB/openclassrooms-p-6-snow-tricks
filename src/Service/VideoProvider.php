<?php

namespace App\Service;

class VideoProvider
{
    private array $video_providers;

    public function __construct(array $video_providers)
    {
        $this->video_providers = $video_providers;
    }

    public function getInfos($url): array
    {
        $provider = $this->getProvider($url);
        $providedId = $this->getProvidedId($provider, $url);

        return array_merge(
            ['provider' => $provider, 'provided_id' => $providedId],
            $this->getProvidedInfos($providedId, $provider)
        );
    }

    public function getProvider($video_url): string
    {
        $provider = '';
        foreach ($this->video_providers as $key => $values) {
            foreach ($values as $v) {
                if (false !== strpos($video_url, $v)) {
                    $provider = $key;
                }
            }
        }

        return $provider;
    }

    public function getProvidedId($provider, $url): string
    {
        $providedId = '';
        switch ($provider) {
            case 'youtube':
                if ((preg_match('/(.+)youtube\.com\/watch\?v=([\w-]+)/', $url, $matches)) ||
                    (preg_match('/(.+)youtu.be\/([\w-]+)/', $url, $matches))
                    ) {
                    $providedId = $matches[2];
                }
                break;
            case 'dailymotion':
                if ((preg_match('/(.+)dailymotion.com\/video\/([\w-]+)/', $url, $matches)) ||
                    (preg_match('/(.+)dai.ly\/([\w-]+)/', $url, $matches))
                ) {
                    $providedId = $matches[2];
                }
                break;
            case 'vimeo':
                if (preg_match('/https:\/\/vimeo.com\/([\w-]+)/', $url, $matches)) {
                    $providedId = $matches[1];
                }
                break;
        }

        return $providedId;
    }

    public function getProvidedInfos($providedId, $provider): array
    {
        $provided_infos = [];
        switch ($provider) {
            case 'youtube':
                $json = json_decode(@file_get_contents(
                    'https://www.youtube.com/oembed?url=https://www.youtube.com/watch?v='.$providedId.'&format=json'
                ));
                if (!empty($json)) {
                    $provided_infos['title'] = $json->title;
                    $provided_infos['thumb_url'] = 'https://img.youtube.com/vi/'.$providedId.'/mqdefault.jpg';
                    $provided_infos['player'] = 'video_box/_embed_ytube.html.twig';
                }
                break;
            case 'vimeo':
                $json = json_decode(@file_get_contents('https://vimeo.com/api/v2/video/'.$providedId.'.json'));
                if (!empty($json)) {
                    $provided_infos['title'] = $json[0]->title;
                    $provided_infos['thumb_url'] = $json[0]->thumbnail_medium;
                    $provided_infos['player'] = 'video_box/_embed_vimeo.html.twig';
                }
                break;
            case 'dailymotion':
                $json = json_decode(@file_get_contents(
                    'https://api.dailymotion.com/video/'.$providedId.'?fields=thumbnail_480_url,title'
                ));
                if (!empty($json)) {
                    $provided_infos['title'] = $json->title;
                    $provided_infos['thumb_url'] = $json->thumbnail_480_url;
                    $provided_infos['player'] = 'video_box/_embed_dmotion.html.twig';
                }
                break;
        }

        return $provided_infos;
    }
}
