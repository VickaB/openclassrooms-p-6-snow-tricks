<?php


namespace App\Service\Factory;

class GalleryFactory
{
    private string $class;

    public function __construct(string $class)
    {
        $this->class = $class;
    }

    public function create()
    {
        return new $this->class;
    }
}
