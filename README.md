# P6 Snowtricks

A simple web app made with Symfony framework:

This scholar project aim to offer a community place for snowboard addicts to share tricks and tips.

Symfony only and no other bundles are used here. Notice that for dev and test purppose, bundle like fzaninotto/Faker are allowed so I used it.

The main Symfony components are detailed in composer.json at project's root.

It also uses Bootstrap 4 framework and JQuery, see the package.json.

## Prerequisite

For install, you will need (presuming this is a local Windows 10 install):

* [Wamp](https://www.wampserver.com/) (if Debian/Ubuntu you will need [Apache](https://httpd.apache.org/), [mariaDB](https://mariadb.org/) and [phpMyAdmin](https://www.phpmyadmin.net/)

* [Composer](https://getcomposer.org/).

* [NodeJs](https://nodejs.org/en/download/).

* [Yarn](https://yarnpkg.com/).

You can use the symfony server to test or create your virtual host if you go local.

## Installation

1. Download zip or clone repository from [Gitlab](https://gitlab.com/oc-da-php-symfony-projects/p6-snowtricks)

2. Run `composer install` to get the dependencies installed. 

This is if you have composer installed globally, if not there is a composer.phar file at project's root you can use too by running `php composer.phar install` .

3. Create database with phpMyadmin or whatever tool you like

4. Enter your database and smtp configuration option in .env file at project's root

5. Run `php bin/console make:migration` in terminal, then `php bin/console doctrine:migration:migrate` this will get your database ready with tables you need.

6. Run `yarn install` so you get the node modules and dependancies from package.json

7. Run `yarn dev` so your webpack can build needed assets and directories

8. Run `php bin/console doctrine:fixture:load` to get demo dataset installed

## Usage

Go to your-url.com create your account, verify your email and start riding!

[Demo here](https://www.snowtricks.ananzebatanga.com)

[![Codacy Badge](https://app.codacy.com/project/badge/Grade/90c06e45398944aa8767ca9a70cfad8a)](https://www.codacy.com/gl/oc-da-php-symfony-projects/p6-snowtricks/dashboard?utm_source=gitlab.com&amp;utm_medium=referral&amp;utm_content=oc-da-php-symfony-projects/p6-snowtricks&amp;utm_campaign=Badge_Grade)

## Test

Run `php bin/phpunit` result shown in terminal.


## Thanks!

This project is a scholar project.

Thank you [OpenClassrooms](https://openclassrooms.com/fr/) for giving anybody anywhere some progress opportunities, thank you my Mentor Thomas Gérault for your kind but demanding leading and thank you my Boss at work for letting me go back to school!

Let me learn more!

Love it!


